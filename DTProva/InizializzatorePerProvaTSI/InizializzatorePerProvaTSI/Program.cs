﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Azure;
using Azure.DigitalTwins.Core;
using Azure.Identity;

namespace InizializzatorePerProvaTSI
{
    class Program
    {
        // Hostname URL
        private static string adtInstanceUrl = "https://dtprova.api.wcus.digitaltwins.azure.net";

        static async Task Main(string[] args)
        {
            DigitalTwinsClient client;
            try
            {
                // Get the credential from local (so CLI "az login" or Visual Studio ecc...)
                DefaultAzureCredential credentials = new DefaultAzureCredential();
                // Log into the service
                client = new DigitalTwinsClient(new Uri(adtInstanceUrl), credentials);
                Console.WriteLine("Logged");
                Dictionary<string, List<string>> dict = await Initialize(client);
                await Populate(client, dict);
            }
            catch (Exception e)
            {
                Console.WriteLine("Authentication or client creation error: " + e.Message);
                Environment.Exit(0);
            }
        }


        private async static Task Populate(DigitalTwinsClient client, Dictionary<string, List<string>> dict)
        {
            Random rnd = new Random();

            //Insert 20 values in each twin
            for (int i = 0; i < 20; i++)
            {
                //Iterate over twins
                foreach (KeyValuePair<string, List<string>> entry in dict)
                {
                    //Update room twin
                    JsonPatchDocument roomPatch = new JsonPatchDocument();
                    roomPatch.AppendAdd("/visitorCount", rnd.Next(20));
                    roomPatch.AppendAdd("/temperature", rnd.NextDouble() * 35);
                    await client.UpdateDigitalTwinAsync(entry.Key, roomPatch);
                    Console.WriteLine($"Aggiornata Room: {entry.Key}");

                    //Update list of devices connected
                    foreach (string devId in entry.Value)
                    {
                        JsonPatchDocument devPatch = new JsonPatchDocument();
                        devPatch.AppendAdd("/value", rnd.Next(30));
                        await client.UpdateDigitalTwinAsync(devId, devPatch);
                        Console.WriteLine($"\tAggiornato Device: {devId}");
                    }
                }
            }
        }

        private async static Task<Dictionary<string, List<string>>> Initialize(DigitalTwinsClient client)
        {
            var dict = new Dictionary<string, List<string>>();
            Console.WriteLine("---INIZIALIZZAZIONE---");
            //Create 10 rooms and attach 2 device to it

            //Create 10 rooms
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                int number = rnd.Next(1000);
                string roomId = "patientRoom" + number;
                BasicDigitalTwin twinData = new BasicDigitalTwin()
                {
                    Id = roomId,
                    Metadata = { ModelId = "dtmi:com:contoso:PatientRoom;2" },
                    //Initialize
                    Contents =
                    {
                        { "visitorCount", 0 },
                        { "handWashCount", 0 },
                        { "handWashPercentage", 0 },
                        //Not initialize temperature { "temperature", 0 }
                    }
                };
                try
                {
                    Response<BasicDigitalTwin> response = await client.CreateOrReplaceDigitalTwinAsync<BasicDigitalTwin>(roomId, twinData);
                    Console.WriteLine("Creata Room: " + roomId);

                    //Create relationship with the hospital
                    string relHospitalId = "hospitalhas" + roomId;
                    BasicRelationship relHospData = new BasicRelationship()
                    {
                        TargetId = roomId,
                        Name = "hasRoom",
                        Properties = { }
                    };
                    await client.CreateOrReplaceRelationshipAsync<BasicRelationship>("hospital", relHospitalId, relHospData);
                    Console.WriteLine(roomId + " legata all'hospital");

                    var deviceList = new List<string>();

                    //Now that is created, add two device
                    for (int j = 0; j < 2; j++)
                    {
                        string deviceId = "device" + number + "-" + j;
                        BasicDigitalTwin deviceData = new BasicDigitalTwin
                        {
                            Id = deviceId,
                            Metadata = { ModelId = "dtmi:com:contoso:Device;1" },
                            //Initialize properties
                            Contents =
                        {
                            { "deviceId", deviceId },
                            //Not initialize value { "value", 0 }
                        },
                        };
                        Response<BasicDigitalTwin> deviceCreateResponse = await client.CreateOrReplaceDigitalTwinAsync<BasicDigitalTwin>(deviceId, deviceData);
                        Console.WriteLine("\tCreato Device: " + deviceId);

                        //Now create relationship
                        BasicRelationship relData = new BasicRelationship()
                        {
                            TargetId = deviceId,
                            Name = "hasDevices",
                            Properties = { }
                        };
                        string relId = roomId + "has" + deviceId;
                        await client.CreateOrReplaceRelationshipAsync<BasicRelationship>(roomId, relId, relData);
                        Console.WriteLine("\t\tCreata Relazione: " + relId);

                        deviceList.Add(deviceId);
                    }

                    dict.Add(roomId, deviceList);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine($"ERROR: {roomId} gia' esistente, provo un altro");
                    //Twin Id already present, generate another
                    //i--;
                }
            }
            return dict;
        }

    }
}
