using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Microsoft.Extensions.Logging;
using Azure.DigitalTwins.Core;
using Azure.DigitalTwins.Core.Serialization;
using Azure.Identity;
using System.Net.Http;
using Azure.Core.Pipeline;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace My.Function
{
    public class TwinsFunction
    {
        //Your Digital Twins URL is stored in an application setting in Azure Functions.
        private static readonly string adtInstanceUrl = Environment.GetEnvironmentVariable("ADT_SERVICE_URL");
        private static readonly HttpClient httpClient = new HttpClient();

        [FunctionName("TwinsFunction")]
        public async void Run([EventGridTrigger] EventGridEvent eventGridEvent, ILogger log)
        {
            log.LogInformation(eventGridEvent.Data.ToString());
            if (adtInstanceUrl == null) log.LogError("Application setting \"ADT_SERVICE_URL\" not set");
            try
            {
                //Authenticate with Digital Twins.
                ManagedIdentityCredential cred = new ManagedIdentityCredential("https://digitaltwins.azure.net");
                DigitalTwinsClient client = new DigitalTwinsClient(new Uri(adtInstanceUrl), cred, new DigitalTwinsClientOptions { Transport = new HttpClientTransport(httpClient) });
                log.LogInformation($"Azure digital twins service client connection created.");
                if (eventGridEvent != null && eventGridEvent.Data != null)
                {
                    log.LogInformation(eventGridEvent.Data.ToString());
                    
                     /*
                        {
                            "properties": {
                                "temperatureAlert": "false"
                            },
                            "systemProperties": {
                                "iothub-content-type": "application/json",
                                "iothub-content-encoding": "utf-8",
                                "iothub-connection-device-id": "MyDotnetDevice",
                                "iothub-connection-auth-method": "{\"scope\":\"device\",\"type\":\"sas\",\"issuer\":\"iothub\",\"acceptingIpFilterRule\":null}",
                                "iothub-connection-auth-generation-id": "637545070752972264",
                                "iothub-enqueuedtime": "2021-04-20T09:54:36.361Z",
                                "iothub-message-source": "Telemetry"
                            },
                            "body": {
                                "temperature": 21.402598082275407,
                                "humidity": 70.51614863356396,
                                "open": false
                            }
                            }
                    */

                    // Read deviceId and temperature for IoT Hub JSON.
                    JObject deviceMessage = (JObject)JsonConvert.DeserializeObject(eventGridEvent.Data.ToString());
                    string deviceId = (string)deviceMessage["systemProperties"]["iothub-connection-device-id"];
                    var temperature = deviceMessage["body"]["temperature"];
                    var humidity = deviceMessage["body"]["humidity"];
                    var open = deviceMessage["body"]["open"];

                    //Update twin by using device temperature.
                    var patch = new Azure.JsonPatchDocument();
                    patch.AppendAdd<double>("/temperature", temperature.Value<double>()); 
                    patch.AppendAdd<double>("/humidity", humidity.Value<double>()); 
                    patch.AppendAdd<bool>("/open", open.Value<bool>()); 

                    await client.UpdateDigitalTwinAsync(deviceId, patch);
                }
            }
            catch (Exception e)
            {
                log.LogError(e.Message);
            }

        }
    }
}