using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using System.Text;

namespace Provahttptrigger
{
    public static class HttpExample
    {

        private static ServiceClient serviceClient;
        private static string connectionString = "HostName=iothubprova1.azure-devices.net;SharedAccessKeyName=service;SharedAccessKey=ojlNc9e8qDVG9DmLjtcXSFslQOmG9+rJKYYhA3Mjn1Y=";
        private static string targetDevice = "MyDotnetDevice";

        [FunctionName("HttpExample")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);
            string action = req.Query["action"];

            string responseMessage = "";

            if(!string.IsNullOrEmpty(action))
            {
                if(action == "open" || action == "close")
                {
                    await sendCommand(action);
                    responseMessage = $"Ok action = {action}";
                } else
                {
                    responseMessage = "Pass an action between: close, open";
                }
            } else
            {
                responseMessage = "Pass an action between: close, open";
            }

            await serviceClient.CloseAsync();
            return new OkObjectResult(responseMessage);
        }

        private static Task sendCommand(string action)
        {
            bool open = action == "open";
            var commandMessage = new Message(Encoding.ASCII.GetBytes($"{{\"open\": {open.ToString().ToLower()}}}"));
            return serviceClient.SendAsync(targetDevice, commandMessage);
        }
    }
}

