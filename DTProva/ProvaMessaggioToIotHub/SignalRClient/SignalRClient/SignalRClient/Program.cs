﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;

namespace SignalRClient
{
    class Program
    {
        private static HubConnection connection;

        static async Task Main(string[] args)
        {
            connection = new HubConnectionBuilder()
                .WithUrl("https://provafunctionapp.azurewebsites.net/api")
                .Build();
            await connection.StartAsync();
            Console.WriteLine("Connection started");

            connection.On<string>("newMessage", (message) =>
            {
                Console.WriteLine($"Received: {message}");
            });

            Console.ReadLine();
        }
    }
}
