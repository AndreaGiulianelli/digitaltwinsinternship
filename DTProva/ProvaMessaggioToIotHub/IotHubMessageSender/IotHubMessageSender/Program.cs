﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;

namespace IotHubMessageSender
{
    class Program
    {
        private static ServiceClient serviceClient;
        private static string connectionString = "HostName=iothubprova1.azure-devices.net;SharedAccessKeyName=service;SharedAccessKey=ojlNc9e8qDVG9DmLjtcXSFslQOmG9+rJKYYhA3Mjn1Y=";
        private static string targetDevice = "MyDotnetDevice";

        static void Main(string[] args)
        {
            Console.WriteLine("Send Cloud-to-Device message\n");
            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);

            Console.WriteLine("0 (or any) -> close,  1-> open");
            string openStr = Console.ReadLine();
            bool open = openStr == "1";
            SendCloudToDeviceMessageAsync(open).Wait();
            Console.WriteLine($"Sent! -> {open.ToString().ToLower()}");
            Console.ReadLine();
        }

        public static Task SendCloudToDeviceMessageAsync(bool open)
        {
            var commandMessage = new Message(Encoding.ASCII.GetBytes($"{{\"open\": {open.ToString().ToLower()}}}"));
            return serviceClient.SendAsync(targetDevice, commandMessage);
        }
    }
}
