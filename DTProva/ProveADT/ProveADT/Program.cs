﻿using System;
//Needed to handle Azure Login 
using Azure.Identity;
//Needed to handle Digital Twins
using Azure.DigitalTwins.Core;
using System.IO;
using System.Threading.Tasks;
using Azure;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace ProveADT
{
    class Program
    {
        // Hostname URL
        private static string adtInstanceUrl = "https://dtprova.api.wcus.digitaltwins.azure.net";
        private static readonly HttpClient httpClient = new HttpClient(); // HttpClient is intended to be instantiated once per application, rather than per-use.
        private static string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;

        static async Task Main(string[] args)
        {
            DigitalTwinsClient client;
            try
            {
                // Get the credential from local (so CLI "az login" or Visual Studio ecc...)
                DefaultAzureCredential credentials = new DefaultAzureCredential();
                // Log into the service
                client = new DigitalTwinsClient(new Uri(adtInstanceUrl), credentials);
                Console.WriteLine("Logged");
                while (true)
                {
                    // Here we are logged.
                    Console.WriteLine("Select action:");
                    int action = Convert.ToInt32(Console.ReadLine());
                    await Dispatcher(client, action);
                }
            } 
            catch(Exception e)
            {
                Console.WriteLine("Authentication or client creation error: " + e.Message);
                Environment.Exit(0);
            }
        }

        private async static Task<bool> Dispatcher(DigitalTwinsClient client, int number)
        {
            switch(number)
            {
                case 1:
                    // Upload Model to Azure Digital Twin
                    Console.Write("Insert model name (.\\models): ");
                    string modelName = Console.ReadLine();
                    string dtdl = "";
                    try
                    {
                        dtdl = File.ReadAllText(Path.Combine(projectDirectory, "models", modelName));
                    } catch(FileNotFoundException e)
                    {
                        Console.WriteLine("File not found, exit");
                        Environment.Exit(0);
                    }

                    Console.WriteLine("Upload this model...");
                    Console.WriteLine(dtdl);
                    await client.CreateModelsAsync(new[] { dtdl });
                    Console.WriteLine("Uploaded");
                    break;
                case 2:
                    // List Models Available
                    // Get models and metadata for a model ID, including all dependencies (models that it inherits from, components it references)
                    // Withoud parameters it returns the simplest model list.
                    AsyncPageable<DigitalTwinsModelData> mds = client.GetModelsAsync(new GetModelsOptions { IncludeModelDefinition = true });
                    await foreach(DigitalTwinsModelData md in mds)
                    {
                        Console.WriteLine($"Id: {md.Id}");
                    }
                    break;
                case 3:
                    // Create a digital Twin
                    // I will use the BasicDigitalTwin to store the twin, but it could be manual defined
                    string twinID = "patientRoom1";
                    BasicDigitalTwin data = new BasicDigitalTwin
                    {
                        Id = twinID,
                        Metadata = { ModelId = "dtmi:com:contoso:PatientRoom;1" },
                        //Initialize properties
                        Contents =
                        {
                            { "visitorCount",25.0},
                            { "handWashCount", 20.0 },
                            { "handWashPercentage", 20.0/25.0 * 100 },
                        },
                    };

                    Response<BasicDigitalTwin> response = await client.CreateOrReplaceDigitalTwinAsync<BasicDigitalTwin>(twinID, data);
                    Console.WriteLine("Creato");
                    Console.WriteLine($"Visitors Count last update on: {response.Value.Metadata.PropertyMetadata["visitorCount"].LastUpdatedOn}");
                    break;
                case 4:
                    // Create a digital Twin
                    // I will use the BasicDigitalTwin to store the twin, but it could be manual defined
                    // This time create a twin that has a component
                    // Component HAS to be initialized when created.
                    string planetID = "planet10";
                    BasicDigitalTwin dataPlanet = new BasicDigitalTwin
                    {
                        Id = planetID,
                        Metadata = { ModelId = "dtmi:com:contoso:Planet;1" },
                        //Initialize properties
                        Contents =
                        {
                            { "provaDateTime", "1985-04-12T23:20:50.52Z"},
                            { "provaTimeSpan", 10 },
                            { "provaBoolean", true },
                            // component
                            {
                                "deepestCrater",
                                new BasicDigitalTwinComponent
                                {
                                    // component properties
                                    Contents =
                                    {
                                        { "componentProp", 5 }
                                    },
                                }
                            },
                        },
                    };

                    Response<BasicDigitalTwin> res = await client.CreateOrReplaceDigitalTwinAsync<BasicDigitalTwin>(planetID, dataPlanet);
                    Console.WriteLine("Creato");
                    Console.WriteLine($"Duration last update on: {res.Value.Metadata.PropertyMetadata["provaDuration"].LastUpdatedOn}");
                    break;
                case 5:
                    // Get Data from a Twin
                    BasicDigitalTwin resTwin; // Use the helper class to deserialize twin in JSON format that it's returned
                    Response<BasicDigitalTwin> twinResponse = await client.GetDigitalTwinAsync<BasicDigitalTwin>("patientRoom1");
                    resTwin = twinResponse.Value;
                    // Only properties that have been set at least once are returned when you retrieve a twin with the GetDigitalTwin() method.
                    // Access metadata
                    Console.WriteLine($"Model id: {resTwin.Metadata.ModelId}");
                    // Get all properties
                    foreach (string prop in resTwin.Contents.Keys)
                    {
                        if (resTwin.Contents.TryGetValue(prop, out object value))
                            Console.WriteLine($"Property '{prop}': {value}");
                    }
                    break;
                case 6:
                    // Update properties of a twin
                    /* To update properties of a digital twin, you write the information you want to replace in JSON Patch format.
                     * In this way, you can replace multiple properties at once.
                     * You then pass the JSON Patch document into an UpdateDigitalTwin() method: */
                    JsonPatchDocument patch = new JsonPatchDocument();
                    patch.AppendAdd("/visitorCount", 30);

                    await client.UpdateDigitalTwinAsync("patientRoom1", patch);
                    Console.WriteLine("Aggiornato");

                    // Update a Digital Twin Component
                    patch = new JsonPatchDocument();
                    patch.AppendAdd("/deepestCrater/componentProp", 10);
                    patch.AppendAdd("/provaDateTime", "1985-04-12T22:20:50.52Z");
                    patch.AppendAdd("/provaBoolean", false);

                    await client.UpdateDigitalTwinAsync("planet10", patch);
                    Console.WriteLine("Aggiornato");

                    break;
                case 7:
                    // Create relationships
                    
                    // First create a device twin
                    Random rnd = new Random();
                    string deviceID = "device" + rnd.Next(100) + rnd.Next(100);
                    BasicDigitalTwin deviceData = new BasicDigitalTwin
                    {
                        Id = deviceID,
                        Metadata = { ModelId = "dtmi:com:contoso:Device;1" },
                        //Initialize properties
                        Contents =
                        {
                            { "deviceId",deviceID + rnd.Next(100)},
                            { "value", 33.0 },
                        },
                    };
                    Response<BasicDigitalTwin> deviceCreateResponse = await client.CreateOrReplaceDigitalTwinAsync<BasicDigitalTwin>(deviceID, deviceData);

                    // Create relationship between Room and the Device
                    BasicRelationship relData = new BasicRelationship()
                    {
                        TargetId = deviceID,
                        Name = "hasDevices",
                        Properties = { }, // In this case it's a relationship without properties
                    };
                    string srcId = "patientRoom1";
                    string relId = srcId + "has" + deviceID;
                    await client.CreateOrReplaceRelationshipAsync<BasicRelationship>(srcId, relId, relData);
                    
                    Console.WriteLine("Relationship created");
                    break;
                case 8:
                    // Get list of relationships (it's also possible to get Incoming Relationships with GetIncomingRelationships)
                    AsyncPageable<BasicRelationship> result = client.GetRelationshipsAsync<BasicRelationship>("patientRoom1");
                    var responses = new List<Task<Response<BasicDigitalTwin>>>();
                    await foreach(BasicRelationship br in result)
                    {
                        Console.WriteLine($"Found relationship: {br.Id}");
                        Console.WriteLine($"Target: {br.TargetId}");
                        // Can iterate also over the properties, but this relationship hasn't

                        // Get info on twin related
                        Task<Response<BasicDigitalTwin>> relTwin = client.GetDigitalTwinAsync<BasicDigitalTwin>(br.TargetId);
                        responses.Add(relTwin);
                    }

                    //Get twins informations
                    foreach(Task<Response<BasicDigitalTwin>> resp in responses)
                    {
                        var singleTwinResponse = await resp;
                        var singleTwin = singleTwinResponse.Value;
                        Console.WriteLine($"ID: {singleTwin.Id}, deviceID: {singleTwin.Contents["deviceId"]},  value: {singleTwin.Contents["value"]}");
                    }
                    break;
                case 9:
                    //Delete a Twin
                    Console.WriteLine("Inserisci nome twin");
                    String id = Console.ReadLine();
                    try
                    {
                        await CustomMethod_DeleteTwinAsync(client, id);
                    } catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case 10:
                    //Query the Time Series Insights to get historical data about a twin.
                    //We need two main info: 1) the FQDN (Fully Qualified Domain Name, the id of the service); 2) the Bearer token in order to authenticate to the API
                    //String bearerToken = "<token>"; Token to access TSI APIs, the token last about 5-60 minutes
                    //Use this command to generate token: az account get-access-token --resource 120d688d-1518-4cf7-bd38-182f158850b6 
                    String fqdn = "a968ccb8-310d-4cfb-8d2f-9e2eb1ca9ba3.env.timeseries.azure.com";
                    String bearerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyIsImtpZCI6Im5PbzNaRHJPRFhFSzFqS1doWHNsSFJfS1hFZyJ9.eyJhdWQiOiIxMjBkNjg4ZC0xNTE4LTRjZjctYmQzOC0xODJmMTU4ODUwYjYiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9lOTk2NDdkYy0xYjA4LTQ1NGEtYmY4Yy02OTkxODFiMzg5YWIvIiwiaWF0IjoxNjE4OTQxMjc5LCJuYmYiOjE2MTg5NDEyNzksImV4cCI6MTYxODk0NTE3OSwiYWNyIjoiMSIsImFpbyI6IkFTUUEyLzhUQUFBQXBzRjRCNENtWUlkakg1RVFPbEw5TXNISGNnRnZyeVJpbXJ4T3hkRGptcTQ9IiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6IjA0YjA3Nzk1LThkZGItNDYxYS1iYmVlLTAyZjllMWJmN2I0NiIsImFwcGlkYWNyIjoiMCIsImZhbWlseV9uYW1lIjoiR2l1bGlhbmVsbGkiLCJnaXZlbl9uYW1lIjoiQW5kcmVhIiwiZ3JvdXBzIjpbIjZhNmMxNWFiLTNiZDgtNDE5Ny04NzQzLTFmYWI5YjBmZDQxYSIsIjczYTU3MjFkLTA1ZDEtNDBjMS05ODA4LWRhMDA0ZTFmNGQxNiIsIjAwZTQ3MWVjLWNjMzUtNDY3Yy05ZmExLWQ5YTg5OTk0MWFmMCIsImE2ZTJhMjA5LTI2ZTUtNGEwNS04Y2FhLTY1MjIzZjJmM2I3OCIsIjNlZDRmZjY4LTQ2NTQtNDk1ZC05MTkxLWEyODc5NGJiYzhmZCIsIjZlYzM4ZTRmLWY3ZDMtNGVhOS04NmIzLTU0MGViOTExYTY0OSIsIjg4OWIwMDhiLWRlMjMtNDczNS04NzJhLTllODQ2MTZkNjJiMiIsImQ0YmIwYTM2LTBkMGEtNDBkZS04NzdjLTIwYzFmMTU1NTA0ZiIsImNkM2YwMGJmLTQ5N2EtNDNhMC1hYTMwLThkMGRjMjBkOGRjYiIsIjhjYjk5MzhmLTc2MjgtNDc3Ny05ZmQ5LTJjNzRhMTc3ZDBhZSIsIjE0OTIyZjE4LTAwMTQtNGJlMi05ZmFmLTFlY2YyZTk2MGU2MyIsImRmNGViYTYyLWI3NGMtNGVlYS1iYjhlLWE5YzJjYzgwMDJjYSIsIjAzMjE1OWI3LWY3NjktNDc4Mi1hMDU4LTQwZWE0NDA0MTZmNCIsImM3YmM5MDFkLTQ4Y2EtNGMyNC05ODU1LWJjYzNkYzM3YWU2ZiIsIjNhZDk1MWJmLTE3YWYtNDk1Mi1iYzMzLWNjNGVhYjc5ZjUxNyIsIjdjNGNkNDFlLTc4OGMtNGEwYi05NjY3LWY0MDExMDUzMGZhMSIsIjk5NTJlNGNmLWM2N2QtNDcyZC04YmExLThkYTJhMzQ5OTE2ZiIsIjA2NDZmNjM0LWFkMmUtNDVjOS1iM2U5LWJjNzk3YzVhZDU3MyIsImE0NmMzNDRhLTdhNzEtNDFhNi1hMDA3LTg3OTE0NzY4NDU2OSIsIjc3YzFkMDg1LTVmNjMtNDQ3Ny05OTk3LTU4MzgzNWUwNGU1YSIsIjkwODdmODBkLTRhZTQtNDMwNy04MmJmLTk5ZDVjZDU5NmFmNCIsImUyNTlhOTI2LWI1NTItNDlmYy1iNWU3LWVkZjk1MjU5MGNlNyIsIjc3YWVkMjAyLTE1NDItNGIzYy1hNWRjLTIxNjM4ZGUyZmZkYiIsIjlmOWY5Y2IxLWE3MTEtNDdjNS05YTdkLTIyYmU4YmQwNjM1MCIsIjNkNzMyNDM5LTgwMTQtNGNkOC1iNGQ5LWNlM2E2NzAwZTQ0MyIsImNlMTRiOGE0LWZlZTAtNGYzYy04ZDQwLWMzNTk5NGVkNzcxMiIsImMyNjk1ZjE1LTFhMTEtNGJhMC1hMmIzLTlhZTkzN2VmOTViMiIsImU0MDBlY2IwLTZmNWMtNDhhYi1hMDBhLTI0YTUxNzczNzUzZSIsImZlNjI1Yzk2LTlhZjUtNDI4My1iN2YwLWE2MjI1YjU0ZDZkNCIsIjUxZDY4MGY5LTUzNGItNGQ0ZS1iMjBiLTVhN2U4NzcxYjNkMyIsIjI4YTM3ZTMyLWM4ODAtNDYzYi1iNDU0LTliNjNkN2M5M2VhZSIsIjE5MjNmM2Q3LThlMWMtNDAwOS04Mzc3LTI1YmY2ZmNkZDQ1ZCIsIjJmNWRlOWIzLTU0NjctNGNlMC1hNmJjLTg0ZGEwMWQ3ZjJjYiIsImNmYmE4MWViLWZjYzYtNDQzNC1iYWQ3LWJhOTU4NTg4NzYwZCIsImJiNTM4ZTg3LTI2ZDctNDE5Ny05OTY4LWNmNWViYmE5ZDUzMiIsImRkZmI2OTczLTI5YTQtNDIwZC05YzFiLWZiNjY4YzllNGQ4MCJdLCJpcGFkZHIiOiI4Mi41Ny4xNzQuMTE0IiwibmFtZSI6IkFuZHJlYSBHaXVsaWFuZWxsaSAtIGFuZHJlYS5naXVsaWFuZWxsaTRAc3R1ZGlvLnVuaWJvLml0Iiwib2lkIjoiNDQ3ZDVkMmEtYWU1NS00YzBhLTliMzMtY2RkZDkzYzNhMzhmIiwib25wcmVtX3NpZCI6IlMtMS01LTIxLTc5MDUyNTQ3OC0xMDM1NTI1NDQ0LTY4MjAwMzMzMC0xNjc4NTk1IiwicHVpZCI6IjEwMDMwMDAwQUM2NDU3MjUiLCJyaCI6IjAuQVFVQTNFZVc2UWdiU2tXX2pHbVJnYk9KcTVWM3NBVGJqUnBHdS00Qy1lR19lMFlGQUx3LiIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6ImlJSjlwdzBMdXJPUFN2Nk5PSDFEZ1d4aFFFU1htZkswcGtWSUR2aTE3Y28iLCJ0aWQiOiJlOTk2NDdkYy0xYjA4LTQ1NGEtYmY4Yy02OTkxODFiMzg5YWIiLCJ1bmlxdWVfbmFtZSI6ImFuZHJlYS5naXVsaWFuZWxsaTRAc3R1ZGlvLnVuaWJvLml0IiwidXBuIjoiYW5kcmVhLmdpdWxpYW5lbGxpNEBzdHVkaW8udW5pYm8uaXQiLCJ1dGkiOiJpMVA4Y2xGRmprNnZwRDlZT0lkT0FBIiwidmVyIjoiMS4wIn0.d5jE68Y5M_ESAmy--i9lYR6wXP_2cwyguH7dnWi2tjLxCFejUhk_SCsvdn_Xih-Phr5D3DletCDw9Ypv0lAAsf0vspqeyYzGHwUzvpCSPs2Lm6l0uYeGRzdWDqVNtOASVw0tIL4C3mrmnGgOHns-liZMqXSGKGfprURAOIjnreyey9UHeaDbZ998-wAYRRgNJkzUyzDK71smfvfNlFJ2jgVj0sPxRnx0t9nME15dZabKyL5dcFECNLePO_-Z0Ojm0NmwumiftL1kPJkLI08N-WIwqdS4ipDZytE2G3i-JlFV2-yAq4ISeBRE82tkGpuBjtRi5bNR2Swf_XLLNDs2ug";

                    //Set the default token for every request
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);
                    String url = $"https://{fqdn}/timeseries/query?api-version=2020-07-31&storeType=coldstore";
                    try
                    {
                        String requerstBodyString = File.ReadAllText(Path.Combine(projectDirectory, "requestBody.json"));
                        HttpContent requestBody = new StringContent(requerstBodyString, Encoding.UTF8, "application/json");
                        HttpResponseMessage responseMsg = await httpClient.PostAsync(url, requestBody);
                        Console.WriteLine(responseMsg.StatusCode);
                        Console.WriteLine(await responseMsg.Content.ReadAsStringAsync());
                        var obj = JObject.Parse(await responseMsg.Content.ReadAsStringAsync()); //Può tirare eccezione "Unauthorized" quando il token è scaduto
                        List<string> timestamps = obj["timestamps"].ToObject<List<string>>();
                        var objDeserialized = obj["properties"].ToObject < List < TSIProperties <double?>>>();
                        foreach(TSIProperties<double?> prop in objDeserialized)
                        {
                            Console.WriteLine("Values: ");
                            var timestampedValues = timestamps.Zip(prop.values, (k, v) => new { Timestamp = k, Value = v });
                            foreach(var x in timestampedValues)
                            {
                                Console.WriteLine(x.Timestamp + " : " + x.Value);
                            }
                            Console.WriteLine("\nName: " + prop.name);
                            Console.WriteLine("\nType: " + prop.type);
                        }
                    } catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case 11:
                    patch = new JsonPatchDocument();
                    patch.AppendAdd("/provaEnum", 2);
                    patch.AppendAdd("/provaMap", new
                    {
                        color = "red",
                        moduleB = "stopped"
                    });

                    await client.UpdateDigitalTwinAsync("prova10", patch);
                    Console.WriteLine("Aggiornato");
                    break;
                case 12:
                    // Send telemetry (la Azure function non ha il filtro attivato su questo quindi non verrà ricevuto)
                    await client.PublishTelemetryAsync("planet10", Guid.NewGuid().ToString(), "{\"Temperature\": 30.5}");
                    Console.WriteLine("Telemetry aggiornata");
                    break;
            }

            return true;
        }

        private static async Task CustomMethod_DeleteTwinAsync(DigitalTwinsClient client, string twinId)
        {
            await CustomMethod_FindAndDeleteOutgoingRelationshipsAsync(client, twinId);
            await CustomMethod_FindAndDeleteIncomingRelationshipsAsync(client, twinId);
            try
            {
                await client.DeleteDigitalTwinAsync(twinId);
                Console.WriteLine("Twin deleted successfully");
            }
            catch (RequestFailedException ex)
            {
                Console.WriteLine($"*** Error:{ex.Message}");
            }
        }

        private static async Task CustomMethod_FindAndDeleteOutgoingRelationshipsAsync(DigitalTwinsClient client, string dtId)
        {
            // Find the relationships for the twin

            try
            {
                // GetRelationshipsAsync will throw an error if a problem occurs
                AsyncPageable<BasicRelationship> rels = client.GetRelationshipsAsync<BasicRelationship>(dtId);

                await foreach (BasicRelationship rel in rels)
                {
                    await client.DeleteRelationshipAsync(dtId, rel.Id).ConfigureAwait(false);
                    Console.WriteLine($"Deleted relationship {rel.Id} from {dtId}");
                }
            }
            catch (RequestFailedException ex)
            {
                Console.WriteLine($"*** Error {ex.Status}/{ex.ErrorCode} retrieving or deleting relationships for {dtId} due to {ex.Message}");
            }
        }

        private static async Task CustomMethod_FindAndDeleteIncomingRelationshipsAsync(DigitalTwinsClient client, string dtId)
        {
            // Find the relationships for the twin

            try
            {
                // GetRelationshipsAsync will throw an error if a problem occurs
                AsyncPageable<IncomingRelationship> incomingRels = client.GetIncomingRelationshipsAsync(dtId);

                await foreach (IncomingRelationship incomingRel in incomingRels)
                {
                    await client.DeleteRelationshipAsync(incomingRel.SourceId, incomingRel.RelationshipId).ConfigureAwait(false);
                    Console.WriteLine($"Deleted incoming relationship {incomingRel.RelationshipId} from {dtId}");
                }
            }
            catch (RequestFailedException ex)
            {
                Console.WriteLine($"*** Error {ex.Status}/{ex.ErrorCode} retrieving or deleting incoming relationships for {dtId} due to {ex.Message}");
            }
        }
    }
}
