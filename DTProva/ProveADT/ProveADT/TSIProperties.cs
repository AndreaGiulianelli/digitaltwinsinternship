﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProveADT
{
    // Simple class to deserialize JSON
    // THERE ISN'T ANY PURPOSE OF GOOD-DESIGN, IT'S BAD DESIGNED AND NOT GENERIC.
    class TSIProperties<T>
    {
        public List<T> values { get; set; }

        public string name { get; set; }

        public string type { get; set; }
    }
}
