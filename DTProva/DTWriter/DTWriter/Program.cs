﻿using Azure;
using Azure.Core;
using Azure.DigitalTwins.Core;
using Azure.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DTWriter
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string adtInstanceUrl = "https://dtprova.api.wcus.digitaltwins.azure.net";
            string tenantId = "e99647dc-1b08-454a-bf8c-699181b389ab";
            string clientId = "437df52c-81f2-4acb-a3b6-daf3c4c87eb7";
            string clientSecret = "D2zfThfp2euNr-.LmcQyos-8SolyS0IQ01";
            DigitalTwinsClient client;

            try
            {
                client = GetDigitalTwinsClient(tenantId, clientId, clientSecret, adtInstanceUrl);
                Console.WriteLine("Logged");

                Console.WriteLine("\n\n---------PROVA LETTURA------------");

                /*
                 * Prova di lettura di un twin.
                 * Visto che l'app AAD creata ha i permessi di lettura/scrittura sull'istanza Azure Digital Twin
                 * protrà accedere in lettura.
                 */

                //Sia alla parte di Control Plane (quindi modelli ecc..)
                AsyncPageable<DigitalTwinsModelData> mds = client.GetModelsAsync(new GetModelsOptions { IncludeModelDefinition = true });
                await foreach (DigitalTwinsModelData md in mds)
                {
                    Console.WriteLine($"Id: {md.Id}");
                }

                //Sia alla parte di Data Plane (quindi twin, relazioni e query)
                await ReadTwin(client);
                await GetRelationship(client);
                await Query(client);


                Console.WriteLine("\n\n---------PROVA SCRITTURA------------");
                /*
                 * Prova di creazione di un twin.
                 * Visto che l'app AAD creata ha anche i permessi di scrittura sull'istanza Azure Digital Twin
                 * non ci saranno problemi.
                 */
                try
                {
                    await TryCreate(client);
                    Console.WriteLine("Creato");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Errore");
                }

                // Prova aggiornamento
                try
                {
                    JsonPatchDocument patch = new JsonPatchDocument();
                    patch.AppendAdd("/visitorCount", 30);

                    await client.UpdateDigitalTwinAsync("patientRoom1", patch);
                    Console.WriteLine("Aggiornato");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Errore");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: NOT logged");
                Console.WriteLine(e.Message);
            }

        }

        private static async Task Query(DigitalTwinsClient client)
        {
            AsyncPageable<BasicDigitalTwin> result = client.QueryAsync<BasicDigitalTwin>("SELECT * FROM DIGITALTWINS");
            try
            {
                await foreach (BasicDigitalTwin twin in result)
                {
                    // You can include your own logic to print the result
                    // The logic below prints the twin's ID and contents
                    Console.WriteLine($"\nTwin ID: {twin.Id} \nTwin data");
                    foreach (KeyValuePair<string, object> kvp in twin.Contents)
                    {
                        Console.WriteLine($"{kvp.Key}  {kvp.Value}");
                    }
                }
            }
            catch (RequestFailedException ex)
            {
                Console.WriteLine($"Error {ex.Status}, {ex.ErrorCode}, {ex.Message}");
                throw;
            }
        }

        private static async Task GetRelationship(DigitalTwinsClient client)
        {
            // Get list of relationships (it's also possible to get Incoming Relationships with GetIncomingRelationships)
            AsyncPageable<BasicRelationship> result = client.GetRelationshipsAsync<BasicRelationship>("patientRoom1");
            Console.WriteLine("Lettura andata a buon fine");
            await foreach (BasicRelationship br in result)
            {
                Console.WriteLine($"Found relationship: {br.Id}");
                Console.WriteLine($"Target: {br.TargetId}");
            }
        }

        private static async Task ReadTwin(DigitalTwinsClient client)
        {
            BasicDigitalTwin resTwin; // Use the helper class to deserialize twin in JSON format that it's returned
            Response<BasicDigitalTwin> twinResponse = await client.GetDigitalTwinAsync<BasicDigitalTwin>("patientRoom1");
            resTwin = twinResponse.Value;
            // Only properties that have been set at least once are returned when you retrieve a twin with the GetDigitalTwin() method.
            // Access metadata
            Console.WriteLine($"Model id: {resTwin.Metadata.ModelId}");
            // Get all properties
            foreach (string prop in resTwin.Contents.Keys)
            {
                if (resTwin.Contents.TryGetValue(prop, out object value))
                    Console.WriteLine($"Property '{prop}': {value}");
            }
        }

        private static async Task TryCreate(DigitalTwinsClient client)
        {
            Random rnd = new Random();
            string twinID = "patientRoom" + rnd.Next(100000);
            BasicDigitalTwin data = new BasicDigitalTwin
            {
                Id = twinID,
                Metadata = { ModelId = "dtmi:com:contoso:PatientRoom;1" },
                //Initialize properties
                Contents =
                        {
                            { "visitorCount",25.0},
                            { "handWashCount", 20.0 },
                            { "handWashPercentage", 20.0/25.0 * 100 },
                        },
            };

            Response<BasicDigitalTwin> response = await client.CreateOrReplaceDigitalTwinAsync<BasicDigitalTwin>(twinID, data);
            Console.WriteLine("Creato");
            Console.WriteLine($"Visitors Count last update on: {response.Value.Metadata.PropertyMetadata["visitorCount"].LastUpdatedOn}");
        }

        private static DigitalTwinsClient GetDigitalTwinsClient(string tenantId, string clientId, string clientSecret, string adtEndpoint)
        {
            // These environment variables are necessary for DefaultAzureCredential to use application Id and client secret to login.
            Environment.SetEnvironmentVariable("AZURE_CLIENT_SECRET", clientSecret);
            Environment.SetEnvironmentVariable("AZURE_CLIENT_ID", clientId);
            Environment.SetEnvironmentVariable("AZURE_TENANT_ID", tenantId);

            #region Snippet:DigitalTwinsSampleCreateServiceClientWithClientSecret

            // DefaultAzureCredential supports different authentication mechanisms and determines the appropriate credential type based of the environment it is executing in.
            // It attempts to use multiple credential types in an order until it finds a working credential.
            TokenCredential tokenCredential = new DefaultAzureCredential();

            var client = new DigitalTwinsClient(
                new Uri(adtEndpoint),
                tokenCredential);

            #endregion Snippet:DigitalTwinsSampleCreateServiceClientWithClientSecret

            return client;
        }
    }
}
