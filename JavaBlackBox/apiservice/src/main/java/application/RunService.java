package application;

import io.vertx.core.Vertx;
import verticles.ApiVerticle;
import verticles.SignalRVerticle;

/**
 * Start point for the Service.
 *
 */
public final class RunService {

    private RunService() { }
    /**
     * @param strings params
     */
    public static void main(final String...strings) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ApiVerticle());
        vertx.deployVerticle(new SignalRVerticle());
    }
}
