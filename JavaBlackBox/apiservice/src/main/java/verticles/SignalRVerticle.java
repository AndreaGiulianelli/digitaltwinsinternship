package verticles;

import java.util.ArrayList;
import java.util.List;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import io.vertx.core.json.JsonObject;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;

public class SignalRVerticle extends AbstractVerticle {

    /**
     * Event bus address for signalr event.
     */
    public static final String EVENT_BUS_SIGNALR_NEW_EVENT = "event_bus_signalr_new_event";
    private final String signalRUrl = "https://functionappadtproject.azurewebsites.net/api";

    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        HubConnection hubConnection = HubConnectionBuilder
                .create(this.signalRUrl)
                .build();

        //After building and before start the connection, define the hub listeners
        hubConnection.on("newMessage", (message) -> {
            System.out.println("New Message from SignalR: " + message);
            this.getId(message).forEach((id) -> {
                eventBus.publish(SignalRVerticle.EVENT_BUS_SIGNALR_NEW_EVENT + "." + id, message);
            });
        }, String.class);

        hubConnection.start().doOnComplete(() -> {
            System.out.println("Connected to SignalR!");

        }).doOnError((e) -> {
            System.out.println("Error SignalR: " + e);
        }).subscribe();
    }

    /*
     * Get the ids interested in the notification.
     * In case of a relationship there are two ids.
     */
    private List<String> getId(final String message) {
        List<String> ids = new ArrayList<>();
        JsonObject json = new JsonObject(message);
        if (json.containsKey("EventType")) {
            switch (json.getString("EventType")) {
                case "Microsoft.DigitalTwins.Relationship.Delete":
                case "Microsoft.DigitalTwins.Relationship.Create":
                        JsonObject body = json.getJsonObject("data");
                        ids.add(body.getString("$sourceId"));
                        ids.add(body.getString("$targetId"));
                    break;
                default:
                    if (json.containsKey("Id")) {
                        ids.add(json.getString("Id"));
                    }
                    break;
            }
        } else {
            System.out.println("Not specified EventType");
        }
        return ids;
    }

}
