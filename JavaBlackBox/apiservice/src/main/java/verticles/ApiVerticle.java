package verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import model.Patient;
import utility.AdtCrud;

public class ApiVerticle extends AbstractVerticle {
    private static final int PORT = 8080;
    private static final int STATUS_BAD_REQUEST = 400;
    private static final int STATUS_CREATED = 201;
    private static final int STATUS_OK = 200;

    private final AdtCrud adtCrud = new AdtCrud();

    @Override
    public final void start() {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.post("/api/twins/patient/create/").handler(this::createPatientTwin);
        router.delete("/api/twins/:twinId/").handler(this::deleteTwin);
        router.get("/api/twins/:twinId/").handler(this::getTwin);
        router.get("/api/twins/:twinId/events/").handler(this::getTwinEvents);
        //Check if adtCrud can connect to adt.
        if (!this.adtCrud.connect()) {
            System.out.println("Impossible to connect to ADT instance, abort.");
            return;
        }
        vertx.createHttpServer()
             .requestHandler(router)
             .listen(ApiVerticle.PORT);
        System.out.println("Http Server on");
    }

    private void getTwinEvents(final RoutingContext routingContext) {
        System.out.println("Received request!");
        final EventBus eventBus = vertx.eventBus();
        HttpServerResponse response = routingContext.response();
        final String id = routingContext.pathParam("twinId");
        routingContext.request().toWebSocket()
        .onSuccess(ws -> {
            System.out.println("Created socket for twin: " + id);
            eventBus.consumer(SignalRVerticle.EVENT_BUS_SIGNALR_NEW_EVENT + "." + id, message -> {
                //Write message to the websocket
                ws.writeTextMessage(message.body().toString());
            });
        }).onFailure((e) -> {
            response.setStatusCode(ApiVerticle.STATUS_BAD_REQUEST).end();
        });
    }

    private void getTwin(final RoutingContext routingContext) {
        System.out.println("Received request!");
        HttpServerResponse response = routingContext.response();
        final String id = routingContext.pathParam("twinId");
        //Now in order to simplify, get the twin as a string and return that.
        this.adtCrud.getTwin(id, String.class).future().onComplete((res) -> {
            System.out.println("Completed! Get twin: " + id);
            response.putHeader("content-type", "application/json")
            .setStatusCode(ApiVerticle.STATUS_OK)
            .end(res.result());
        }).onFailure((e) -> {
            System.out.println("Error in the creation: " + e);
            response.putHeader("content-type", "application/json")
            .setStatusCode(ApiVerticle.STATUS_BAD_REQUEST)
            .end("{\"status\" : \"Error\"}");
        });
    }

    private void deleteTwin(final RoutingContext routingContext) {
        System.out.println("Received request!");
        HttpServerResponse response = routingContext.response();
        final String id = routingContext.pathParam("twinId");
        this.adtCrud.deleteTwin(id).future().onSuccess(res -> {
            //deleted correctly
            System.out.println("Completed! Deleted twin: " + id);
            response.putHeader("content-type", "application/json")
            .setStatusCode(ApiVerticle.STATUS_OK)
            .end("{\"status\" : 1}");
        }).onFailure(res -> {
            //error
            System.out.println("Error in the creation: " + res);
            response.putHeader("content-type", "application/json")
            .setStatusCode(ApiVerticle.STATUS_BAD_REQUEST)
            .end("{\"status\" : \"Error or already deleted\"}");
        });
    }

    private void createPatientTwin(final RoutingContext routingContext) {
        /*
         * Create a Patient twin, with only ID and code.
         */
        System.out.println("Received request!");
        HttpServerResponse response = routingContext.response();
        try {
            JsonObject jsonBody = routingContext.getBodyAsJson();
            //response.putHeader("Access-Control-Allow-Origin", "*");
            final Patient patient = new Patient();
            patient.setFromJsonObject(jsonBody);
            final String modelId = "dtmi:com:contoso:patient;1";
            final Patient.PatientMetadata patientMetadata = new Patient.PatientMetadata();
            patientMetadata.setModelId(modelId);
            patient.setMetadata(patientMetadata);
            this.adtCrud.<Patient>createTwin(patient, Patient.class).future().onSuccess(res -> {
                //Request completed
                System.out.println("Completed! Created twin: " + res.getId());
                response.putHeader("content-type", "application/json")
                .setStatusCode(ApiVerticle.STATUS_CREATED)
                .end("{\"status\" : 1}");
            }).onFailure((res) -> {
                //Request failed
                System.out.println("Error in the creation: " + res);
                response.putHeader("content-type", "application/json")
                .setStatusCode(ApiVerticle.STATUS_BAD_REQUEST)
                .end("{\"status\" : 0}");
            });
            System.out.println("sent");
        } catch (Exception e) {
            System.out.println("exception: " + e);
            response.putHeader("content-type", "application/json")
            .setStatusCode(ApiVerticle.STATUS_BAD_REQUEST)
            .end("{\"status\" : 0}");
        }
    }

}
