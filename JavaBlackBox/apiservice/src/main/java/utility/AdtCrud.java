package utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.azure.digitaltwins.core.BasicRelationship;
import com.azure.digitaltwins.core.DigitalTwinsAsyncClient;
import com.azure.digitaltwins.core.DigitalTwinsClientBuilder;
import com.azure.identity.ClientSecretCredentialBuilder;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Promise;
import model.Twin;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

public class AdtCrud {
    private final String hostName = "https://adthome.api.neu.digitaltwins.azure.net/";
    private final String tenantId = "e99647dc-1b08-454a-bf8c-699181b389ab";
    private final String appId = "701fd8fe-952e-4992-970e-07423e2467b2";
    private final String appSecret = "3dW.u2_.JusJoIBaGBOetlrb_Vw30YDE4r";
    private DigitalTwinsAsyncClient digitalTwinsAsyncClient;

    public AdtCrud() {
    }

    /**
     * @return status of connection: true->connected, false->error.
     */
    public boolean connect() {
        /*
         * It's may be a long-running task
         */
        try {
            //Connect to the Azure Digital Twins instance
            this.connectToAdt();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }
    private void connectToAdt() {
        //If the connection has a problem it will throw an exception.
        this.digitalTwinsAsyncClient = new DigitalTwinsClientBuilder()
                .credential(
                    new ClientSecretCredentialBuilder()
                        .tenantId(this.tenantId)
                        .clientId(this.appId)
                        .clientSecret(this.appSecret)
                        .build())
                .endpoint(this.hostName)
                .buildAsyncClient();
        System.out.println("connected to adt");
    }

    /**
     * @param <T> the type of the twin passed.
     * @param twin the effective twin to be created
     * @param twinClass the class of the twin.
     * @return a promise for the creation of the twin. Add handler in order to see the result.
     */
    public <T extends Twin> Promise<T> createTwin(final T twin, final Class<T> twinClass) {
        final Promise<T> promise = Promise.promise();
        try {
            this.digitalTwinsAsyncClient.<T>createOrReplaceDigitalTwin(twin.getId(), twinClass.cast(twin), twinClass)
            .subscribe(response -> {
                try {
                    System.out.println("Created digital twin Id: " + response.getId());
                    promise.complete(response);
                } catch (Exception e) {
                    System.out.println(e);
                    promise.fail("Error");
                }
            });
        } catch (Exception e) {
            System.out.println(e);
            promise.fail("Error");
        }
        return promise;
    }

    /**
     * @param <T> the twin class type
     * @param id the twin id
     * @param twinClass the twin class obj
     * @return a promise in order to get the twin async.
     */
    public <T> Promise<T> getTwin(final String id, final Class<T> twinClass) {
        final Promise<T> promise = Promise.promise();
        try {
            this.digitalTwinsAsyncClient.getDigitalTwin(id, twinClass).doOnSuccess((res) -> {
                promise.complete(res);
            }).doOnError((e) -> {
                promise.fail(e);
            }).subscribe();
        } catch (Exception e) {
            System.out.println("Error: " + e);
            promise.fail(e);
        }
        return promise;
    }

    /**
     * @param id the id of the twin
     * @return a promise for the deletion of the twin.
     */
    public Promise<Void> deleteTwin(final String id) {
        final Promise<Void> promise = Promise.promise();
        //In order to delete a twin we need to delete all the relationships before.
        CompositeFuture.all(this.deleteIncomingRelationShips(id).future(), this.deleteOutgoingRelationShips(id).future())
            .onComplete(res -> {
                if (res.succeeded()) {
                    try {
                        //Then delete the twin
                        this.digitalTwinsAsyncClient.deleteDigitalTwin(id).doOnSuccess(result -> {
                            //if OK, complete the promise
                            promise.complete();
                        })
                        .doOnError(result -> {
                            promise.fail("Error in eliminating twin");
                        }).subscribe();
                    } catch (Exception e) {
                        promise.fail("Error");
                    }
                } else {
                    promise.fail("failed to delete relationships");
                }
            });
        return promise;
    }

    private Promise<Void> deleteIncomingRelationShips(final String id) {
        final Promise<Void> promise = Promise.promise();
        List<Mono<Void>> delTasks = new ArrayList<>();
        try {
            this.digitalTwinsAsyncClient.listIncomingRelationships(id)
            .doOnNext(incomingRel -> {
                //Add task to delete this relationship
                delTasks.add(this.digitalTwinsAsyncClient.deleteRelationship(id, incomingRel.getRelationshipId()).subscribeOn(Schedulers.boundedElastic()));
            })
            .doAfterTerminate(() -> {
                //When we start all the delete tasks, subscribe to them and when all completed we can complete the promise.
                if (!delTasks.isEmpty()) {
                    System.out.println("Deleted Incoming");
                    Mono.when(delTasks).subscribe(x -> promise.complete());
                } else {
                    System.out.println("Incoming rel empty");
                    promise.complete();
                }
            }).subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail("Error");
        }
        return promise;
    }
 
    private Promise<Void> deleteOutgoingRelationShips(final String id) {
        final Promise<Void> promise = Promise.promise();
        List<Mono<Void>> delTasks = new ArrayList<>();
        try {
            this.digitalTwinsAsyncClient.listRelationships(id, BasicRelationship.class)
            .doOnNext(outgoingRel -> {
                //Add task to delete this relationship
                delTasks.add(this.digitalTwinsAsyncClient.deleteRelationship(id, outgoingRel.getId()).subscribeOn(Schedulers.boundedElastic()));
            }).doAfterTerminate(() -> {
                //When we start all the delete tasks, subscribe to them and when all completed we can complete the promise.
                if (!delTasks.isEmpty()) {
                    System.out.println("Deleted Outgoing");
                    Mono.when(delTasks).subscribe(x -> promise.complete());
                } else {
                    System.out.println("Outgoing rel empty");
                    promise.complete();
                }
            }).subscribe();
        } catch (Exception e) {
            System.out.println(e);
            promise.fail("Error");
        }
        return promise;
    }
}

