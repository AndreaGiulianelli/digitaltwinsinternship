package model;

/**
 * Code for patient gravity.
 *
 */
public enum PatientCode {
    /**
     * red = 0.
     */
    RED,
    /**
     * yellow = 1.
     */
    YELLOW,
    /**
     * green = 2.
     */
    GREEN,
    /**
     * white = 3.
     */
    WHITE
}
