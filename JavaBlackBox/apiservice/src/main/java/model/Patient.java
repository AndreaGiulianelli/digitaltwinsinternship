package model;

import com.azure.core.models.JsonPatchDocument;
import com.azure.digitaltwins.core.DigitalTwinPropertyMetadata;
import com.azure.digitaltwins.core.models.DigitalTwinsJsonPropertyNames;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonObject;

/**
 * Model of the patient.
 * It serves also as a serializer/deserializer for Azure Digital Twins SDK.
 */
public class Patient extends AbstractTwin {
    @JsonProperty("heartRate")
    private int heartRate;
    @JsonProperty("bloodPressure")
    private double bloodPressure;
    @JsonProperty("bodyTemperature")
    private double bodyTemperature;
    @JsonProperty("patientCode")
    private int patientCode;

    @JsonProperty(DigitalTwinsJsonPropertyNames.DIGITAL_TWIN_METADATA)
    private PatientMetadata metadata = new PatientMetadata();

    /*
     * Json patch document that will be built during the set of the object.
     * In this way we can abstract the name used in the digital twin instance.
     */
    private transient JsonPatchDocument patchDocument = new JsonPatchDocument();

    /**
     * @return heartRate.
     */
    public int getHeartRate() {
        return this.heartRate;
    }
    /**
     * @param heartRate
     */
    public void setHeartRate(final int heartRate) {
        this.heartRate = heartRate;
        this.patchDocument.appendAdd("/heartRate", heartRate);
    }
    /**
     * @return blood pressure.
     */
    public double getBloodPressure() {
        return this.bloodPressure;
    }
    /**
     * @param bloodPressure
     */
    public void setBloodPressure(final double bloodPressure) {
        this.bloodPressure = bloodPressure;
        this.patchDocument.appendAdd("/bloodPressure", bloodPressure);
    }
    /**
     * @return body temperature.
     */
    public double getBodyTemperature() {
        return this.bodyTemperature;
    }
    /**
     * @param bodyTemperature
     */
    public void setBodyTemperature(final double bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
        this.patchDocument.appendAdd("/bodyTemperature", bodyTemperature);
    }
    /**
     * @return patientCode in integer format.
     */
    public int getPatientCode() {
        return this.patientCode;
    }
    /**
     * @param code
     */
    public void setPatientCode(final int code) {
        this.patientCode = code;
        this.patchDocument.appendAdd("/patientCode", patientCode);
    }
    /**
     * @return metadata
     */
    public PatientMetadata getMetadata() {
        return this.metadata;
    }
    /**
     * @param metadata metadata of twin.
     */
    public void setMetadata(final PatientMetadata metadata) {
        this.metadata = metadata;
    }
    /**
     * @return the enum version of PatientCode
     */
    public PatientCode getPatientCodeEnum() {
        //Get the enum version
        return PatientCode.values()[this.patientCode];
    }
    /**
     * @param obj that contains parameters.
     */
    public void setFromJsonObject(final JsonObject obj) {
        if (obj.containsKey("id")) {
            this.setId(obj.getString("id"));
        }
        if (obj.containsKey("bloodPressure")) {
            this.setBloodPressure(obj.getDouble("bloodPressure"));
        }
        if (obj.containsKey("bodyTemperature")) {
            this.setBodyTemperature(obj.getDouble("bodyTemperature"));
        }
        if (obj.containsKey("heartRate")) {
            this.setHeartRate(obj.getInteger("heartRate"));
        }
        if (obj.containsKey("patientCode")) {
            this.setPatientCode(obj.getInteger("patientCode"));
        }
    }
    /**
     * @return the path document for the Patient.
     */
    public JsonPatchDocument getPatch() {
        return this.patchDocument;
    }

    public static class PatientMetadata extends DigitalTwinMetadata {
        @JsonProperty("heartRate")
        private DigitalTwinPropertyMetadata heartRate;
        @JsonProperty("bloodPressure")
        private DigitalTwinPropertyMetadata bloodPressure;
        @JsonProperty("bodyTemperature")
        private DigitalTwinPropertyMetadata bodyTemperature;
        @JsonProperty("patientCode")
        private DigitalTwinPropertyMetadata patientCode;

        /**
         * @return heart rate metadata.
         */
        public DigitalTwinPropertyMetadata getHeartRate() {
            return this.heartRate;
        }
        /**
         * @param heartRate metadata.
         */
        public void setHeartRate(final DigitalTwinPropertyMetadata heartRate) {
            this.heartRate = heartRate;
        }
        /**
         * @return blood pressure metadata.
         */
        public DigitalTwinPropertyMetadata getBloodPressure() {
            return this.bloodPressure;
        }
        /**
         * @param bloodPressure metadata.
         */
        public void setBloodPressure(final DigitalTwinPropertyMetadata bloodPressure) {
            this.bloodPressure = bloodPressure;
        }
        /**
         * @return body temperature metadata.
         */
        public DigitalTwinPropertyMetadata getBodyTemperature() {
            return this.bodyTemperature;
        }
        /**
         * @param bodyTemperature metadata.
         */
        public void setBodyTemperature(final DigitalTwinPropertyMetadata bodyTemperature) {
            this.bodyTemperature = bodyTemperature;
        }
        /**
         * @return patient code (integer) metadata.
         */
        public DigitalTwinPropertyMetadata getPatientCode() {
            return this.patientCode;
        }
        /**
         * @param patientCode metadata.
         */
        public void setPatientCode(final DigitalTwinPropertyMetadata patientCode) {
            this.patientCode = patientCode;
        }
    }
}
