``` Java
private Patient testCreatePatient() {
        //test to create patient
        final Patient patient = new Patient();
        patient.setId("patientTest1");
        patient.setBloodPressure(5);
        patient.setBodyTemperature(36);
        patient.setHeartRate(68);
        patient.setPatientCode(PatientCode.WHITE.ordinal());
        return patient;
    }

    private void testCreatePatientTwin(final Patient patient) {
        /*
         * Model driven version of creating a digital twin
         */
        final String modelId = "dtmi:com:contoso:patient;1";
        final Patient.PatientMetadata patientMetadata = new Patient.PatientMetadata();
        patientMetadata.setModelId(modelId);
        patient.setMetadata(patientMetadata);
        try {
            this.digitalTwinsAsyncClient.createOrReplaceDigitalTwin(patient.getId(), patient, Patient.class)
            .subscribe(response -> {
                try {
                    System.out.println("Created digital twin Id: " + response.getId());
                    System.out.println("Metadata: " + response.getMetadata().getModelId() + " day:" + 
                    response.getMetadata().getHeartRate().getLastUpdatedOn().getDayOfWeek().getValue());
                } catch (Exception e) {
                    System.out.println(e);
                }
            });
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void testCreateAmbulanceTwin() {
        /*
         * Fast way to create a Digital Twin
         */
        String modelId = "dtmi:com:contoso:ambulance;1";
        BasicDigitalTwin basicTwin = new BasicDigitalTwin("provaAmbulance1")
            .setMetadata(
                new BasicDigitalTwinMetadata()
                    .setModelId(modelId)
            );
        class Location {
            @JsonProperty("latitude")
            public double latitude;
            @JsonProperty("longitude")
            public double longitude;
        }
        basicTwin.getContents().put("lastLocation", new Location());

        digitalTwinsAsyncClient.createOrReplaceDigitalTwin(basicTwin.getId(), basicTwin, BasicDigitalTwin.class)
            .subscribe(response -> System.out.println("Created digital twin Id: " + response.getId()));
    }
```