package verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;

public class WebSocketClientVerticle extends AbstractVerticle {

    @Override
    public final void start() {
        HttpClient httpClient = vertx.createHttpClient();
        httpClient.webSocket(8080, "localhost", "/api/twins/Patient1/events/", res -> {
            if (res.succeeded()) {
                WebSocket ws = res.result();
                System.out.println("Connected!");
                ws.frameHandler(frame -> {
                    System.out.println("Received: " + frame.textData());
                });
              }
        });
    }

}
