package model;

import com.azure.digitaltwins.core.models.DigitalTwinsJsonPropertyNames;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DigitalTwinMetadata {
    @JsonProperty(DigitalTwinsJsonPropertyNames.METADATA_MODEL)
    private String modelId;

    /**
     * @return model id of the twin.
     */
    public String getModelId() {
        return this.modelId;
    }

    /**
     * @param modelId of the twin
     */
    public void setModelId(final String modelId) {
        this.modelId = modelId;
    }
}
