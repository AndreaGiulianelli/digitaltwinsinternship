package model;

import com.azure.digitaltwins.core.models.DigitalTwinsJsonPropertyNames;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AbstractTwin implements Twin {
    @JsonProperty(DigitalTwinsJsonPropertyNames.DIGITAL_TWIN_ID)
    private String id;
    @JsonProperty(DigitalTwinsJsonPropertyNames.DIGITAL_TWIN_ETAG)
    private String eTag;

    /**
     * @return the twin id
     */
    public String getId() {
        return this.id;
    }
    /**
     * @param id the twin id
     */
    public void setId(final String id) {
        this.id = id;
    }
    /**
     * @return the twin id
     */
    public String geteTag() {
        return this.eTag;
    }
    /**
     * @param eTag the twin id
     */
    public void seteTag(final String eTag) {
        this.eTag = eTag;
    }
}
