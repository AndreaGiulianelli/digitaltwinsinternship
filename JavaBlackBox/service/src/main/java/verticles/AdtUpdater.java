package verticles;

import com.azure.core.models.JsonPatchDocument;
import com.azure.digitaltwins.core.BasicDigitalTwin;
import com.azure.digitaltwins.core.BasicDigitalTwinMetadata;
import com.azure.digitaltwins.core.DigitalTwinsAsyncClient;
import com.azure.digitaltwins.core.DigitalTwinsClientBuilder;
import com.azure.identity.ClientSecretCredentialBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import model.Patient;
import model.PatientCode;
import reactor.core.publisher.Mono;

public class AdtUpdater extends AbstractVerticle {

    /**
     * Event Bus Address for new data coming.
     */
    public static final String EVENT_BUS_NEW_DATA = "event_bus_new_data_for_adt";

    private final String hostName = "https://adthome.api.neu.digitaltwins.azure.net/";
    private final String tenantId = "e99647dc-1b08-454a-bf8c-699181b389ab";
    private final String appId = "41a85494-7904-4037-a625-4d55213c3db6";
    private final String appSecret = "ThR-vMjw.Yy~IU3UH6d2..AS1_k0JG5C_L";
    private DigitalTwinsAsyncClient digitalTwinsAsyncClient;

    private EventBus eventBus;

    @Override
    public final void start(final Promise<Void> startPromise) {
        Promise<Object> isConnected = Promise.promise();
        this.eventBus = vertx.eventBus();

        vertx.executeBlocking(promise -> {
            try {
                //Connect to the Azure Digital Twins instance
                this.connectToAdt();
                promise.complete();
            } catch (Exception e) {
                promise.fail("Connection error: " + e.getMessage());
            }
        }, res -> {
            System.out.println("Connected to Azure Digital Twins");
            isConnected.complete();
        });

        isConnected.future().onComplete(res -> {
            this.eventBus.consumer(AdtUpdater.EVENT_BUS_NEW_DATA, message -> this.handleMessage(message));
        });
        startPromise.complete();
    }

    private void handleMessage(final Message<Object> message) {
        final JsonObject receivedObject = new JsonObject(message.body().toString());
        switch (receivedObject.getString("MessageType")) {
            case "paziente":
                //For the patient use the custom serializer/deserializer
                final Patient patient = new Patient();
                patient.setFromJsonObject(receivedObject);
                try {
                    this.updateTwin(patient.getId(), patient.getPatch()).doOnSuccess(res -> {
                        System.out.println("Twin id: " + patient.getId() + " updated.");
                    }).subscribe();
                    System.out.println("mandata");
                } catch (Exception e) {
                    System.out.println(e);
                }
            break;
            case "info":
                //For ambulance info only parse JSON data (in order to see different solutions)
                final JsonPatchDocument patch = new JsonPatchDocument();
                if (receivedObject.containsKey("fuel")) {
                    patch.appendAdd("/fuelPercentage", receivedObject.getInteger("fuel"));
                }
                if (receivedObject.containsKey("busy")) {
                    patch.appendAdd("/busy", receivedObject.getBoolean("busy"));
                }
                if (receivedObject.containsKey("latitude")) {
                    patch.appendAdd("/lastLocation/latitude", receivedObject.getDouble("latitude"));
                }
                if (receivedObject.containsKey("longitude")) {
                    patch.appendAdd("/lastLocation/longitude", receivedObject.getDouble("longitude"));
                }
                final String id = receivedObject.getString("id");
                try {
                    this.updateTwin(id, patch).doOnSuccess(res -> {
                        System.out.println("Twin id: " + id + " updated.");
                    }).subscribe();
                    System.out.println("mandata");
                } catch (Exception e) {
                    System.out.println(e);
                }
                break;
            default:
                break;
        }
    }

    private Mono<Void> updateTwin(final String id, final JsonPatchDocument patch) {
        System.out.println(id);
        System.out.println(patch);
        return this.digitalTwinsAsyncClient.updateDigitalTwin(id, patch);
    }

    private void connectToAdt() {
        //If the connection has a problem it will throw an exception.
        this.digitalTwinsAsyncClient = new DigitalTwinsClientBuilder()
                .credential(
                    new ClientSecretCredentialBuilder()
                        .tenantId(this.tenantId)
                        .clientId(this.appId)
                        .clientSecret(this.appSecret)
                        .build())
                .endpoint(this.hostName)
                .buildAsyncClient();
    }
}
