package verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.messages.MqttConnAckMessage;

public class MqttClientVerticle extends AbstractVerticle {

    private static final int MQTT_PORT = 1883;
    private static final String TOPIC = "project/ambulanza/+";
    @Override
    public final void start() {
        final EventBus eventBus = vertx.eventBus();
        final MqttClient client = MqttClient.create(vertx);
        //Connect to broker (I use Mosquitto as broker, in local)
        Future<MqttConnAckMessage> future = client.connect(MQTT_PORT, "localhost");
        //When connection is estabilished subscribe to the topics
        future.onComplete((ar) -> {
            if (ar.succeeded()) {
                client.publishHandler(s -> {
                    System.out.println(s.payload().toString());
                    System.out.println(s.topicName());
                    System.out.println(s.topicName().split("/")[2]);
                    eventBus.send(AdtUpdater.EVENT_BUS_NEW_DATA, s.payload().toJsonObject().put("MessageType", s.topicName().split("/")[2]));
                }).subscribe(TOPIC, 0);
            }
        });
    }

}
