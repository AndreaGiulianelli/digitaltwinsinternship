package application;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public final class Main {
    private static final String TOPIC_INFO = "project/ambulanza/paziente";
    private static final int QOS = 2;
    private static final String BROKER = "tcp://localhost:1883";
    private static final String CLIENT_ID = "client_id";

    private Main() { }
    /**
     * @param strings params
     */
    public static void main(final String...strings) {
        final MemoryPersistence persistence = new MemoryPersistence();
        final String content = "{\n"
                + "  \"id\": \"Patient1\",\n"
                + "  \"heartRate\": 68\n"
                + "}";
        /*final String content = "{\n"
                + "  \"id\": \"provaAmbulance\",\n"
                + "  \"latitude\": 47.5644,\n"
                + "  \"longitude\": 33.32435,\n"
                + "  \"fuel\": 75\n"
                + "}";*/

        try {
            MqttClient sampleClient = new MqttClient(Main.BROKER, Main.CLIENT_ID, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(false);
            System.out.println("Connecting to broker: " + Main.BROKER);
            sampleClient.connect(connOpts);
            System.out.println("Connected");
            System.out.println("Publishing message: " + content);
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(Main.QOS);
            sampleClient.publish(Main.TOPIC_INFO, message);
            System.out.println("Message published");
            sampleClient.disconnect();
            System.out.println("Disconnected");
            System.exit(0);
            sampleClient.close();
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }
}
