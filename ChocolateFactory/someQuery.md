# Link
Alcuni link:
- [https://docs.microsoft.com/en-us/learn/modules/develop-with-azure-digital-twins/build-azure-digital-twins-graph-for-chocolate-factory/8-querying-digital-twin-graphs](https://docs.microsoft.com/en-us/learn/modules/develop-with-azure-digital-twins/build-azure-digital-twins-graph-for-chocolate-factory/8-querying-digital-twin-graphs)
- [https://docs.microsoft.com/en-us/azure/digital-twins/concepts-query-language#reference-expressions-and-conditions](https://docs.microsoft.com/en-us/azure/digital-twins/concepts-query-language#reference-expressions-and-conditions)

# Prove

Query per ottenere tutti i twin di un certo modello (così prende anche tutti quelli che ereditano quel modello)
```
SELECT * FROM DIGITALTWINS WHERE IS_OF_MODEL('dtmi:sample:thing;1')
```
Così prende solo quelli di quel modello esattamente (cioè non considera i sotto-modelli)
```
SELECT * FROM DIGITALTWINS WHERE IS_OF_MODEL('dtmi:sample:thing;1', exact)
```

Query per verificare tutte le relationship del twin con ID:roasting e del tipo rel_step_link

```
SELECT T, CT FROM digitaltwins T JOIN CT RELATED T.rel_step_link WHERE T.$dtId = 'roasting'
```

Query basata sulle proprietà di una relazione.
Prendendo in considerazione una relazione "servicedBy" che ha la proprietà "reportedCondition" (Note how reportedCondition is a property of the servicedBy relationship itself, and not of some digital twin that has a servicedBy relationship.): 
```
SELECT T, SBT, R FROM DIGITALTWINS T JOIN SBT RELATED T.servicedBy R WHERE T.$dtId = 'ABC' AND R.reportedCondition = 'clean'
```

# Query dell'esercizio
```
SELECT  *  FROM DIGITALTWINS T   WHERE T.FinalStep = true
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE IS_DEFINED(T.$dtId)
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE T.$dtId = 'factory'
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE ENDSWITH(T.$dtId,'ing')
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE STARTSWITH(T.$dtId,'f')
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE STARTSWITH(T.$dtId,'prod') OR ENDSWITH(T.$dtId,'r')
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE T.$dtId IN ['factory','grinding']
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE T.ChasisTemperature > 50
```
```
SELECT  *  FROM DIGITALTWINS T   WHERE T.PowerUsage > 100 AND T.ChasisTemperature >= 55
```