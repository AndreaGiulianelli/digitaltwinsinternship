# The scenario
[Link to the tutorial](https://docs.microsoft.com/en-us/learn/modules/develop-with-azure-digital-twins/build-azure-digital-twins-graph-for-chocolate-factory/)

A chocolate manufacturing company, Contoso Chocolate, has one factory and one production line. Sales are going well, and you expect the company to grow. The company wants to determine the best course of action for growing operations while minimizing costs.

For this scenario, the production of chocolate has been simplified into three steps: Roasting, Grinding, and Molding. The following image shows the production line:

![Diagram of a chocolate production line.](./chocolate-production-line.png)

Cocoa beans are the input to this production line, and chocolate bars are the output!

Roasting: The roasting process cooks fermented cocoa beans. The temperature and time of cooking depends on the type of beans, but typically the roasting (sometimes called fanning in the chocolate trade) might take 35 minutes at 120 to 150 degrees Celsius. Roasting aids the removal of unwanted stuff, such as acetic acid, and the formation of the sweet taste of cocoa.

Grinding: The grinding process takes the cocoa nibs that result from the roasting and crushes them to pieces, typically between steel plates, to create a liquid cocoa butter.

Molding: The molding process cools the cocoa butter in molds, giving the desired shape: chocolate bars, egg shells, and figures. The following image shows roasted cocoa nibs:

![Photograph of roasted chocolate nibs.](chocolate-nibs.png)

You've been asked to advise on the correct growth path for the company. You have decided to use Azure Digital Twins to build a software solution flexible enough that no matter what questions the company owners might ask, you'll have a scientific and robust answer.

The first task is to build a digital model of the existing production line. This task is the objective of this module. Further Learn modules take the scenario further, exploring real-world data input, IoT sensors, time series insights, and, ultimately, simulation and advanced analysis.